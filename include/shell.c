#include "shell.h"

string buffer = "\0                                  ";

u8int maincolor = 0x0f;
int hbeer = 0;
void shell_enterpressed(){
	printchar('\n');
	shell();
}

void shell_addchar(char c){
	concatc(buffer,c,buffer);
}

void shell_backspace(){
	remchar(buffer,buffer);
}

int shell_can_backspace(){
	if(strlen(buffer) > 0)
		return 1;
	return 0;
}

void init_shell(){
	setcolor(maincolor);
	print(">");
}

void shell(){
	findCommand(buffer);
	buffer[0] = '\0';
	setcolor(maincolor);
	if(getY() == 0)
		print(">");
	else
		print("\n>");
}

char * simpsons =
"                                 ___    _\n\r"
"                                  | |_||_\n\r"
"      sssSSSSSs                   | | ||_\n\r"
"   sSSSSSSSSSSSs                           sSSSSs             nn   sSSSs\n\r"
"  SSSS           ii  mM     mmm   pPPPPpp sSS           nn    nn sSSSSSS\n\r"
" SSSs           iII mMMMM  mMmmm pPP  PpppSs      oOoo  nNN   nN SS   SS\n\r"
" SSSs           iII mMMMMM mM Mm Pp     PPSSSSs  OOOOOO NNNn nNN SSSs\n\r"
" SSSSSSSssss    iIi mMM MMmM  Mm ppPPppPP  SSSSsoO   OO NNNNNNNN  SSSSss\n\r"
"    SSSSSSSSSs  iIi MMM  MMM  Mm PPPPppP      sSOO   OO NN  nNNN     SSSs\n\r"
"          SSSS  IIi mMM  MMm  Mm  Pp   sSSssSSSSOO ooOO nN   NN        SS\n\r"
"           sSS  III MM       MMm pPp    SSSSSS  OOOOO          sssssSsSS\n\r"
"sSSsssssSSSSS   II                                               SSSSSSS TM\n\r"
"  SSSSSSSSS";

char * beer = 
"         _.._..,_,_\n\r"
"        (          )\n\r"
"         ]~,'-.-~~[\n\r"
"       .=])' (;  ([\n\r"
"       | ]:: '    [\n\r"
"       '=]): .)  ([\n\r"
"         |:: '    |\n\r"
"          ~~----~~\n\r";

char * gggg = 
"          _ _,---._ \n\r"
"       ,-','       `-.___ \n\r"
"      /-;'               `._ \n\r"
"     /\\/          ._   _,'o \\ \n\r"
"    ( /\\       _,--'\\,','´`. ) \n\r"
"     |\\      ,'o     \\'    //\\ \n\r"
"     |      \\        /   ,--'''`-. \n\r"
"     :       \\_    _/ ,-'         `-._ \n\r"
"      \\        `--'  /                ) \n\r"
"       `.  \\`._    ,'     ________,',' \n\r"
"         .--`     ,'  ,--` __\\___,;' \n\r"
"          \\`.,-- ,' ,`_)--'  /`.,' \n\r"
"           \\( ;  | | )      (`-/ \n\r"
"             `--'| |)       |-/ \n\r"
"               | | |        | | \n\r"
"               | | |,.,-.   | |_ \n\r"
"               | `./ /   )---`  ) \n\r"
"              _|  /    ,',   ,-' \n\r"
"             ,'|_(    /-<._,' |--, \n\r"
"             |    `--'---.     \\/ \\ \n\r"
"             |          / \\    /\\  \\ \n\r"
"           ,-^---._     |  \\  /  \\  \\ \n\r"
"        ,-'        \\----'   \\/    \\--`. \n\r"
"       /            \\              \\   \\ \n\r";

char * doh =
"                       _ ,___,-'',-=-. \n\r"
"           __,-- _ _,-'_)_  (''`'-._\\ `. \n\r"
"        _,'  __ |,' ,-' __)  ,-     /. | \n\r"
"      ,'_,--'   |     -'  _)/         `\\ \n\r"
"    ,','      ,'       ,-'_,`           : \n\r"
"    ,'     ,-'       ,(,-(              : \n\r"
"         ,'       ,-' ,    _            ; \n\r"
"        /        ,-._/`---'            / \n\r"
"       /        (____)(----. )       ,' \n\r"
"      /         (      `.__,     /\\ /, \n\r"
"     :           ;-.___         /__\\/| \n\r"
"     |         ,'      `--.      -,\\ | \n\r"
"     :        /            \\    .__/ \n\r"
"      \\      (__            \\    |_ \n\r"
"       \\       ,`-,         /   _|,\\ \n\r"
"        \\    ,'   `-.     ,'_,-'    \\ \n\r"
"       (_\\,-'    ,'\\'')--,'-'       __\\ \n\r"
"        \\       /  // ,'|      ,--'  `-. \n\r"
"         `-.    `-/ \\'  |   _,'         `. \n\r"
"            `-._ /      `--'/             \\ \n\r"
"               ,'           |              \\ \n\r"
"              /             |               \\ \n\r"
"           ,-'              |               / \n\r"
"          /                 |             -' \n\r";

char * bart = 
"       \\  /\\  /\\  /\\  /\\  /\\  /\\  /\n\r"
"        \\/  \\/  \\/  \\/  \\/  \\/  \\/      \n\r"
"        |                        |       H   H  EEEEE  Y   Y\n\r"
"        |                        |       H   H  E      Y   Y\n\r"
"        |                        |       HHHHH  EEE     Y Y\n\r"
"        |                        |       H   H  E        Y\n\r"
"        | __----__      __----__ |       H   H  EEEEE    Y\n\r"
"        |/        \\    /        \\|\n\r"
"        |          |  |          |       M   M   AAA   N   N   !!\n\r"
"      --|\\     *  /    \\     *  /|--     MM MM  A   A  NN  N   !!\n\r"
"     /  | --____--      --____-- |  \\    M M M  AAAAA  N N N   !! \n\r"
"    | (-|         /    \\         |-) |   M   M  A   A  N  NN   \n\r"
"     \\  |.        \\____/        .|  /    M   M  A   A  N   N   ()\n\r"
"      --/                        \\--   \n\r"
"       /                          \\     \n\r"
"       `--______________________--' \n\r"
"          \\_                  _/\n\r"
"            `---__      __---' \n\r"
"                  `----'\n\r";

void findCommand(string command){
    if(strcmp(command,"ping")){
		setcolor(0xf0);
		print("pong!");
		setcolor(0x0f);
	}else if(strcmp(command,"hello homer")){
		if(hbeer == 0){
			print("Hello Alvin, I want a beer");
		} else {
			println(gggg);
			hbeer = 0;
		}
	}else if(strcmp(command,"cls")){
		cls();
	}else if(startswith(command,"setcolor")){
		int a = toHex(command[11]);
		int b = toHex(command[12]);
		if(startswith(command, "setcolor 0x") && strlen(command) == 13 && a != -1 && b != -1){
			maincolor = (a * 16)+b;
			setcolor(maincolor);
			colorRestOfScreen();
			print("color set to 0x");
			printchar(command[11]);
			printchar(command[12]);
		}else{
			setcolor(0x04);
			print("Please specify a color.");
		}
	}else if(strcmp(command,"bart")){
		println(bart);
	}else if(strcmp(command,"beer")){
	println(beer);
	hbeer = 1;
	}else if(strcmp(command,"simpsons")){
		print(simpsons);
	}else if(startswith(command,"new")){
	//	newFile(root);
	}else if(strcmp(command,"help")){
		println("MMMMMMM Donuts");
	}else{
		int bg = maincolor/16;
		if(bg == 0x4)
			setcolor((bg*16)+0xf);
		else
			setcolor((bg*16)+0x4);
		println(doh);
		print("Command not found \"");
		print(command);
		print("\"");
		print(".");
	}
}